resource "aws_instance" "backend-main" {
  provider = "aws.backend"
  count = "${var.instance_count}"
  availability_zone = "${element(var.zones, count.index)}"
  ami = "${data.aws_ami.debian.id}"
  instance_type = "${var.instance_type}"
  ipv6_address_count = 1
  subnet_id = "${element(var.subnets, count.index)}"
  vpc_security_group_ids = ["${var.security_group}"]
  key_name = "waldi" # FIXME

  lifecycle {
    ignore_changes = ["ami", "ebs_optimized", "instance_type"]
  }

  tags {
    Environment = "${var.env}",
    Instance = "${count.index}",
    Name = "mirror-${var.env}-b-m-${var.region_suffix}-${count.index}",
    Project = "mirror",
    Purpose = "backend-main",
  }
}

resource "aws_ebs_volume" "backend-main" {
  provider = "aws.backend"
  count = "${var.instance_count}"
  availability_zone = "${element(var.zones, count.index)}"
  size = "${var.disk_size_main}"
  snapshot_id = "${join("", data.aws_ebs_snapshot.main.*.snapshot_id)}"
  type = "gp2"

  lifecycle {
    ignore_changes = [ "snapshot_id" ]
  }

  tags {
    Environment = "${var.env}",
    Name = "mirror-${var.env}-b-m-${var.region_suffix}-${count.index}-data"
    Project = "mirror",
  }
}

resource "aws_volume_attachment" "backend-main" {
  provider = "aws.backend"
  count = "${var.instance_count}"
  device_name = "/dev/sdb"
  volume_id = "${aws_ebs_volume.backend-main.*.id[count.index]}"
  instance_id = "${aws_instance.backend-main.*.id[count.index]}"
}

resource "aws_alb_target_group_attachment" "backend-main" {
  provider = "aws.backend"
  count = "${var.instance_count}"
  target_group_arn = "${var.target_group_main}"
  target_id = "${aws_instance.backend-main.*.id[count.index]}"
  port = 80
}

resource "aws_route53_record" "backend-main" {
  provider = "aws.global"
  count = "${var.instance_count}"
  zone_id = "${var.route53_zone}"
  name = "b-m-${var.region_suffix}-${count.index}",
  type = "AAAA"
  ttl = 3600
  records = ["${aws_instance.backend-main.*.ipv6_addresses[count.index]}"]
}
