variable "provider_access_key" { }
variable "provider_secret_key" { }
variable "env" { }
variable "region" { }
variable "region_suffix" { }
variable "zones" {
  type = "list"
}

variable "disk_snapshot_main" { }
variable "disk_snapshot_security" { }
variable "disk_size_main" { }
variable "disk_size_security" { }
variable "instance_ami" { }
variable "instance_count" { }
variable "instance_type" { }
variable "route53_zone" { }
variable "security_group" { }
variable "subnets" {
  type = "list"
}
variable "target_group_main" { }
variable "target_group_security" { }
