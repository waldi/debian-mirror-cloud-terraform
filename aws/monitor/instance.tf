resource "aws_instance" "monitor" {
  provider = "aws.monitor"
  availability_zone = "${var.zone}"
  ami = "${data.aws_ami.debian.id}"
  instance_type = "${var.instance_type}"
  ipv6_address_count = 1
  subnet_id = "${var.subnet}"
  vpc_security_group_ids = ["${aws_security_group.monitor.id}"]
  key_name = "waldi" # FIXME

  lifecycle {
    ignore_changes = ["ami"]
  }

  tags {
    Environment = "${var.env}",
    Name = "mirror-${var.env}-m",
    Project = "mirror"
    Purpose = "monitor",
  }
}

resource "aws_route53_record" "monitor" {
  provider = "aws.global"
  zone_id = "${var.route53_zone}"
  name = "m"
  type = "AAAA"
  ttl = 3600
  records = ["${aws_instance.monitor.ipv6_addresses}"]
}
