provider "aws" {
  alias = "global"
  region = "eu-central-1"
  access_key = "${var.provider_access_key}"
  secret_key = "${var.provider_secret_key}"
}
