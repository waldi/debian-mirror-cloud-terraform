variable "provider_access_key" { }
variable "provider_secret_key" { }
variable "env" { }
variable "region" { }
variable "region_suffix" { }
variable "zones" {
  type = "list"
}

variable "lb_count" {}
variable "route53_zone" { }
variable "cidr_blocks" {
  type = "list"
}
