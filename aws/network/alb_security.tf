resource "aws_alb" "security" {
  provider = "aws.network"
  count = "${var.lb_count}"
  name = "mirror-${var.env}-${var.region_suffix}-security"
  ip_address_type = "dualstack"
  security_groups = ["${aws_security_group.lb.id}"]
  subnets = ["${data.aws_subnet.default.*.id}"]

  tags {
    Name = "mirror-${var.env}-security"
    Project = "mirror"
  }
}

resource "aws_alb_listener" "security" {
  provider = "aws.network"
  count = "${var.lb_count}"
  load_balancer_arn = "${aws_alb.security.arn}"
  port = 80
  protocol = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.security.arn}"
    type = "forward"
  }
}

resource "aws_alb_target_group" "security" {
  provider = "aws.network"
  count = "${var.lb_count}"
  name = "mirror-${var.env}-${var.region_suffix}-security"
  port = 80
  protocol = "HTTP"
  vpc_id = "${data.aws_vpc.default.id}"

  health_check {
    path = "/.online"
  }

  tags {
    Name = "mirror-${var.env}-security"
    Project = "mirror"
  }
}

resource "aws_route53_record" "security" {
  provider = "aws.global"
  count = "${var.lb_count}"
  zone_id = "${var.route53_zone}"
  name = "b-s"
  type = "A"
  set_identifier = "backend-${var.region_suffix}"

  alias {
    name = "${aws_alb.security.dns_name}"
    zone_id = "${aws_alb.security.zone_id}"
    evaluate_target_health = false
  }

  latency_routing_policy {
    region = "${var.region}"
  }
}
