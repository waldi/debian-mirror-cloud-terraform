provider "aws" {
  alias = "syncproxy"
  region = "${var.region}"
  access_key = "${var.provider_access_key}"
  secret_key = "${var.provider_secret_key}"
}
