resource "aws_instance" "syncproxy-security" {
  provider = "aws.syncproxy"
  availability_zone = "${var.zone}"
  ami = "${data.aws_ami.debian.id}"
  instance_type = "${var.instance_type}"
  ipv6_address_count = 1
  subnet_id = "${var.subnet}"
  vpc_security_group_ids = ["${aws_security_group.syncproxy.id}"]
  key_name = "waldi" # FIXME

  lifecycle {
    ignore_changes = ["ami"]
  }

  tags {
    Environment = "${var.env}",
    Name = "mirror-${var.env}-s-s",
    Project = "mirror"
    Purpose = "syncproxy-security",
  }
}

resource "aws_ebs_volume" "syncproxy-security" {
  provider = "aws.syncproxy"
  availability_zone = "${var.zone}"
  size = "${var.disk_size_security}"
  snapshot_id = "${join("", data.aws_ebs_snapshot.security.*.snapshot_id)}"
  type = "gp2"

  lifecycle {
    ignore_changes = [ "snapshot_id" ]
  }

  tags {
    Environment = "${var.env}",
    Name = "mirror-${var.env}-s-s-data"
    Project = "mirror"
  }
}

resource "aws_volume_attachment" "syncproxy-security" {
  provider = "aws.syncproxy"
  device_name = "/dev/sdb"
  volume_id = "${aws_ebs_volume.syncproxy-security.id}"
  instance_id = "${aws_instance.syncproxy-security.id}"
}

resource "aws_route53_record" "syncproxy-security" {
  provider = "aws.global"
  zone_id = "${var.route53_zone}"
  name = "s-s"
  type = "AAAA"
  ttl = 3600
  records = ["${aws_instance.syncproxy-security.*.ipv6_addresses[count.index]}"]
}
