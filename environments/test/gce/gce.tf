module "gce" {
  source = "../../../gce"

  provider_project = "debian-infra"
  env = "test"

  disk_snapshot_archive_main = "mirror-main-20171114"
  instance_count_backend = {
    europe-west1 = 2
  }
  instance_image = "debian-cloud/debian-9"
  instance_ip_jump = "35.189.252.57"
  instance_ip_monitor = "35.195.178.1"
  instance_type_backend = "n1-standard-2"
  instance_type_monitor = "g1-small"
  instance_type_syncproxy = "n1-standard-2"
  lb_global_ip = [
    "35.190.93.121",
    "2600:1901:0:6729::",
  ]
  lb_global_ssl_certificate = "${file("snakeoil_b.crt")}"
  lb_global_ssl_private_key = "${file("snakeoil_b.key")}"
  zone_jump = "europe-west1-d"
  zone_monitor = "europe-west1-d"
  zone_syncproxy = "europe-west1-c"
}

terraform {
  backend "gcs" {
    bucket = "debian-infra-mgt"
    prefix = "terraform-state/mirror/test/gce"
  }
}
