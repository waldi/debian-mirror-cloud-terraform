module "aws" {
  source = "../../aws"

  provider_access_key = "${var.aws_access_key}"
  provider_secret_key = "${var.aws_secret_key}"
  env = "test"

  domain = "aws-mirror.const-cast.de"
  disk_size_main = 1500
  disk_size_security = 150
  disk_snapshot_main = ""
  disk_snapshot_security = ""
  disk_snapshot_main = "mirror-main-20170301"
  disk_snapshot_security = "mirror-security-20170301"
  instance_ami = "debian-stretch-hvm-x86_64-gp2-*"
  instance_count_backend = {
    ap-southeast-2 = 0,
    eu-central-1 = 2,
    sa-east-1 = 0,
    us-west-2 = 0,
  }
  instance_type_backend = "m4.xlarge"
  instance_type_monitor = "t2.small"
  instance_type_syncproxy = "m4.xlarge"
  zones = {
    ap-southeast-2 = ["ap-southeast-2a", "ap-southeast-2b"],
    eu-central-1 = ["eu-central-1a", "eu-central-1b"],
    sa-east-1 = ["sa-east-1a", "sa-east-1c"],
    us-west-2 = ["us-west-2a", "us-west-2b"],
  }
}

variable "aws_access_key" {}
variable "aws_secret_key" {}
