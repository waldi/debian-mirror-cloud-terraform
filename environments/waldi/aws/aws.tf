module "aws" {
  source = "../../../aws"

  provider_access_key = "${var.aws_access_key}"
  provider_secret_key = "${var.aws_secret_key}"
  env = "waldi"

  domain = "aws-mirror.reinterpret-cast.de"
  disk_size_main = 20
  disk_size_security = 10
  disk_snapshot_main = ""
  disk_snapshot_security = ""
  instance_ami = "debian-stretch-hvm-x86_64-gp2-*"
  instance_count_backend = {
    ap-southeast-2 = 2,
    eu-central-1 = 2,
    sa-east-1 = 2,
    us-west-2 = 2,
  }
  instance_type_backend = "t2.small"
  instance_type_monitor = "t2.small"
  instance_type_syncproxy = "t2.small"
  zones = {
    ap-southeast-2 = ["ap-southeast-2a", "ap-southeast-2b"],
    eu-central-1 = ["eu-central-1a", "eu-central-1b"],
    sa-east-1 = ["sa-east-1a", "sa-east-1c"],
    us-west-2 = ["us-west-2a", "us-west-2b"],
  }
}

variable "aws_access_key" {}
variable "aws_secret_key" {}
