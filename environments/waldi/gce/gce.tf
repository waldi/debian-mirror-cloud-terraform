module "gce" {
  source = "../../../gce"

  provider_project = "debian-cloud-experiments"
  env = "waldi"

  enable_archive_debug = 1
  enable_archive_ports = 1
  enable_archive_security = 1

  disk_size_archive_debug = 10
  disk_size_archive_main = 20
  disk_size_archive_ports = 10
  disk_size_archive_security = 10
  disk_snapshot_archive_main = ""
  disk_snapshot_archive_ports = ""
  disk_snapshot_archive_security = ""
  instance_count_backend = {
    asia-east1 = 2
    asia-northeast1 = 2
    australia-southeast1 = 2
    europe-west1 = 2
    us-central1 = 2
  }
  instance_image = "debian-cloud/debian-9"
  instance_ip_jump = "35.195.203.253"
  instance_ip_monitor = "35.195.163.127"
  instance_type_backend = "f1-micro"
  instance_type_monitor = "g1-small"
  instance_type_syncproxy = "g1-small"
  lb_global_ip = [
    "35.190.60.39",
    "2600:1901:0:4cb2::",
  ]
  lb_global_ssl_certificate = "${file("snakeoil_b.crt")}"
  lb_global_ssl_private_key = "${file("snakeoil_b.key")}"
  zone_jump = "europe-west1-d"
  zone_monitor = "europe-west1-d"
  zone_syncproxy = "europe-west1-c"
}

terraform {
  backend "gcs" {
    bucket = "debian-infra-mgt"
    prefix = "terraform-state/mirror/waldi/gce"
  }
}
