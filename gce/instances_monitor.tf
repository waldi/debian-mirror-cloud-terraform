resource "google_compute_instance" "monitor" {
  name = "mirror-${var.env}-m"
  zone = "${var.zone_monitor}"
  machine_type = "${var.instance_type_monitor}"
  allow_stopping_for_update = true

  labels = {
    environment = "${var.env}",
    purpose = "monitor",
    project = "mirror",
  }

  tags = [
    "mirror-${var.env}",
    "mirror-${var.env}-monitor",
  ]

  service_account {
    scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring.write",
      "https://www.googleapis.com/auth/pubsub",
    ]
  }

  boot_disk {
    initialize_params {
      image = "${var.instance_image}"
      type = "pd-ssd"
    }
  }

  attached_disk {
    source = "${google_compute_disk.monitor.self_link}"
  }

  network_interface {
    subnetwork = "mirror"
    access_config {
      nat_ip = "${var.instance_ip_monitor}"
    }
  }
}

resource "google_compute_disk" "monitor" {
  name = "mirror-${var.env}-m-data"
  zone = "${var.zone_monitor}"
  size = "${var.disk_size_monitor}"
  type = "pd-ssd"
}
