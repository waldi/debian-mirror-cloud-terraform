resource "google_compute_ssl_certificate" "lb-global" {
  name_prefix = "mirror-${var.env}-b-snakeoil-"
  certificate = "${var.lb_global_ssl_certificate}"
  private_key = "${var.lb_global_ssl_private_key}"

  lifecycle {
    create_before_destroy = true
  }
}
