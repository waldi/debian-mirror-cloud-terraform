resource "google_compute_backend_service" "default" {
  count = "${var.enable}"
  name = "${var.prefix_backend}"
  port_name = "http"
  protocol = "HTTP"
  timeout_sec = 120

  backend {
    group = "${module.backend_ase1a.group[0]}"
  }
  backend {
    group = "${module.backend_ase1c.group[0]}"
  }
  backend {
    group = "${module.backend_asne1a.group[0]}"
  }
  backend {
    group = "${module.backend_asne1b.group[0]}"
  }
  backend {
    group = "${module.backend_ause1a.group[0]}"
  }
  backend {
    group = "${module.backend_ause1b.group[0]}"
  }
  backend {
    group = "${module.backend_euw1c.group[0]}"
  }
  backend {
    group = "${module.backend_euw1d.group[0]}"
  }
  backend {
    group = "${module.backend_usc1c.group[0]}"
  }
  backend {
    group = "${module.backend_usc1f.group[0]}"
  }

  health_checks = ["${var.lb_health_checks}"]

  lifecycle {
    ignore_changes = [
      "enable_cdn",
    ]
  }
}
