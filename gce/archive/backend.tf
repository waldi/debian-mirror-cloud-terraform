locals {
  instance_count_backend_ase1 = "${lookup(var.instance_count_backend, "asia-east1", 0)}"
  instance_count_backend_asne1 = "${lookup(var.instance_count_backend, "asia-northeast1", 0)}"
  instance_count_backend_ause1 = "${lookup(var.instance_count_backend, "australia-southeast1", 0)}"
  instance_count_backend_euw1 = "${lookup(var.instance_count_backend, "europe-west1", 0)}"
  instance_count_backend_usc1 = "${lookup(var.instance_count_backend, "us-central1", 0)}"

  instance_shift_backend_ase1 = 0
  instance_shift_backend_asne1 = "${local.instance_shift_backend_ase1 + local.instance_count_backend_ase1}"
  instance_shift_backend_ause1 = "${local.instance_shift_backend_asne1 + local.instance_count_backend_asne1}"
  instance_shift_backend_euw1 = "${local.instance_shift_backend_ause1 + local.instance_count_backend_ause1}"
  instance_shift_backend_usc1 = "${local.instance_shift_backend_euw1 + local.instance_count_backend_euw1}"
}

module "backend_ase1a" {
  source = "./backend"
  enable = "${var.enable}"
  env = "${var.env}"
  archive = "${var.archive}"
  prefix = "${var.prefix_backend}-ase1a"

  disk_size = "${var.disk_size}"
  disk_snapshot = "${var.disk_snapshot}"
  instance_count = "${ceil(local.instance_count_backend_ase1 / 2.0)}"
  instance_image = "${var.instance_image}"
  instance_shift = "${local.instance_shift_backend_ase1}"
  instance_type = "${var.instance_type_backend}"
  zone = "asia-east1-a"
}

module "backend_ase1c" {
  source = "./backend"
  enable = "${var.enable}"
  env = "${var.env}"
  archive = "${var.archive}"
  prefix = "${var.prefix_backend}-ase1c"

  disk_size = "${var.disk_size}"
  disk_snapshot = "${var.disk_snapshot}"
  instance_count = "${floor(local.instance_count_backend_ase1 / 2.0)}"
  instance_image = "${var.instance_image}"
  instance_shift = "${local.instance_shift_backend_ase1 + 1}"
  instance_type = "${var.instance_type_backend}"
  zone = "asia-east1-c"
}

module "backend_asne1a" {
  source = "./backend"
  enable = "${var.enable}"
  env = "${var.env}"
  archive = "${var.archive}"
  prefix = "${var.prefix_backend}-asne1a"

  disk_size = "${var.disk_size}"
  disk_snapshot = "${var.disk_snapshot}"
  instance_count = "${ceil(local.instance_count_backend_asne1 / 2.0)}"
  instance_image = "${var.instance_image}"
  instance_shift = "${local.instance_shift_backend_asne1}"
  instance_type = "${var.instance_type_backend}"
  zone = "asia-northeast1-a"
}

module "backend_asne1b" {
  source = "./backend"
  enable = "${var.enable}"
  env = "${var.env}"
  archive = "${var.archive}"
  prefix = "${var.prefix_backend}-asne1b"

  disk_size = "${var.disk_size}"
  disk_snapshot = "${var.disk_snapshot}"
  instance_count = "${floor(local.instance_count_backend_asne1 / 2.0)}"
  instance_image = "${var.instance_image}"
  instance_shift = "${local.instance_shift_backend_asne1 + 1}"
  instance_type = "${var.instance_type_backend}"
  zone = "asia-northeast1-b"
}

module "backend_ause1a" {
  source = "./backend"
  enable = "${var.enable}"
  env = "${var.env}"
  archive = "${var.archive}"
  prefix = "${var.prefix_backend}-ause1a"

  disk_size = "${var.disk_size}"
  disk_snapshot = "${var.disk_snapshot}"
  instance_count = "${ceil(local.instance_count_backend_ause1 / 2.0)}"
  instance_image = "${var.instance_image}"
  instance_shift = "${local.instance_shift_backend_ause1}"
  instance_type = "${var.instance_type_backend}"
  zone = "australia-southeast1-a"
}

module "backend_ause1b" {
  source = "./backend"
  enable = "${var.enable}"
  env = "${var.env}"
  archive = "${var.archive}"
  prefix = "${var.prefix_backend}-ause1b"

  disk_size = "${var.disk_size}"
  disk_snapshot = "${var.disk_snapshot}"
  instance_count = "${floor(local.instance_count_backend_ause1 / 2.0)}"
  instance_image = "${var.instance_image}"
  instance_shift = "${local.instance_shift_backend_ause1 + 1}"
  instance_type = "${var.instance_type_backend}"
  zone = "australia-southeast1-c"
}

module "backend_euw1c" {
  source = "./backend"
  enable = "${var.enable}"
  env = "${var.env}"
  archive = "${var.archive}"
  prefix = "${var.prefix_backend}-euw1c"

  disk_size = "${var.disk_size}"
  disk_snapshot = "${var.disk_snapshot}"
  instance_count = "${ceil(local.instance_count_backend_euw1 / 2.0)}"
  instance_image = "${var.instance_image}"
  instance_shift = "${local.instance_shift_backend_euw1}"
  instance_type = "${var.instance_type_backend}"
  zone = "europe-west1-c"
}

module "backend_euw1d" {
  source = "./backend"
  enable = "${var.enable}"
  env = "${var.env}"
  archive = "${var.archive}"
  prefix = "${var.prefix_backend}-euw1d"

  disk_size = "${var.disk_size}"
  disk_snapshot = "${var.disk_snapshot}"
  instance_count = "${floor(local.instance_count_backend_euw1 / 2.0)}"
  instance_image = "${var.instance_image}"
  instance_shift = "${local.instance_shift_backend_euw1 + 1}"
  instance_type = "${var.instance_type_backend}"
  zone = "europe-west1-d"
}

module "backend_usc1c" {
  source = "./backend"
  enable = "${var.enable}"
  env = "${var.env}"
  archive = "${var.archive}"
  prefix = "${var.prefix_backend}-usc1c"

  disk_size = "${var.disk_size}"
  disk_snapshot = "${var.disk_snapshot}"
  instance_count = "${ceil(local.instance_count_backend_usc1 / 2.0 )}"
  instance_image = "${var.instance_image}"
  instance_shift = "${local.instance_shift_backend_usc1}"
  instance_type = "${var.instance_type_backend}"
  zone = "us-central1-c"
}

module "backend_usc1f" {
  source = "./backend"
  enable = "${var.enable}"
  env = "${var.env}"
  archive = "${var.archive}"
  prefix = "${var.prefix_backend}-usc1f"

  disk_size = "${var.disk_size}"
  disk_snapshot = "${var.disk_snapshot}"
  instance_count = "${floor(local.instance_count_backend_usc1 / 2.0)}"
  instance_image = "${var.instance_image}"
  instance_shift = "${local.instance_shift_backend_usc1 + 1}"
  instance_type = "${var.instance_type_backend}"
  zone = "us-central1-f"
}
