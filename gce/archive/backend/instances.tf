resource "google_compute_instance" "default" {
  count = "${var.enable ? var.instance_count : 0}"
  name = "${var.prefix}${count.index}"
  zone = "${var.zone}"
  machine_type = "${var.instance_type}"
  allow_stopping_for_update = true

  labels = {
    archive = "${var.archive}"
    environment = "${var.env}",
    instance = "${(count.index * 2 + var.instance_shift) % 4}",
    purpose = "backend",
    project = "mirror",
  }

  tags = [
    "mirror-${var.env}",
    "mirror-${var.env}-backend",
  ]

  boot_disk {
    initialize_params {
      image = "${var.instance_image}"
      type = "pd-ssd"
    }
  }

  attached_disk {
    source = "${google_compute_disk.default.*.self_link[count.index]}"
  }

  network_interface {
    subnetwork = "mirror"
  }
}

resource "google_compute_disk" "default" {
  count = "${var.enable ? var.instance_count : 0}"
  name = "${var.prefix}${count.index}-data"
  zone = "${var.zone}"
  size = "${var.disk_size}"
  snapshot = "${var.disk_snapshot}"
  type = "pd-ssd"

  lifecycle {
    ignore_changes = [ "snapshot" ]
  }
}

resource "google_compute_instance_group" "default" {
  count = "${var.enable ? 1 : 0}"
  name = "${var.prefix}"
  zone = "${var.zone}"

  named_port {
    name = "http"
    port = "8080"
  }

  instances = [
    "${google_compute_instance.default.*.self_link}"
  ]
}
