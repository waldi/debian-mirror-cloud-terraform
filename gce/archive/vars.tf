variable "enable" { }
variable "env" { }
variable "archive" { }
variable "prefix_backend" { }
variable "prefix_syncproxy" { }

variable "disk_size" { }
variable "disk_snapshot" { }
variable "instance_count_backend" {
  type = "map"
}
variable "instance_image" { }
variable "instance_type_backend" { }
variable "instance_type_syncproxy" { }
variable "lb_health_checks" {
  type = "list"
}
variable "zone_syncproxy" { }
