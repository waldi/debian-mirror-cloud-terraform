resource "google_compute_route" "default-backend" {
  name = "default-route-mirror-${var.env}-b"
  dest_range = "0.0.0.0/0"
  network = "mirror"
  next_hop_instance = "${google_compute_instance.jump.name}"
  next_hop_instance_zone = "${google_compute_instance.jump.zone}"
  tags = [
    "mirror-${var.env}-backend",
  ]
  priority = 100
}
