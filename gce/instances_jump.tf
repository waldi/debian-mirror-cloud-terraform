resource "google_compute_instance" "jump" {
  name = "mirror-${var.env}-j"
  zone = "${var.zone_jump}"
  machine_type = "g1-small"
  allow_stopping_for_update = true

  labels = {
    environment = "${var.env}",
    purpose = "jump",
    project = "mirror",
  }

  tags = [
    "mirror-${var.env}",
    "mirror-${var.env}-jump",
  ]

  boot_disk {
    initialize_params {
      image = "${var.instance_image}"
      type = "pd-ssd"
    }
  }

  can_ip_forward = true
  network_interface {
    subnetwork = "mirror"
    access_config {
      nat_ip = "${var.instance_ip_jump}"
    }
  }
}
